
# -------- Modules/files.awk

function dirname(_dirname_){ 
  sub(/\/[^/]+$/,"",_dirname_)
  return _dirname_
}

function basename(_name_,_suffix_) {
 gsub(/^\/|[^/]+\//,"",_name_)
 if (_suffix_) {
   _suffix_	= index(_name_,_suffix_)-1
   _name_	= substr(_name_,1,_suffix_)
 }
 return _name_
}
